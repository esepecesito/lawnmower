define reflash
file build/LawnMower.elf
monitor init
monitor reset halt
monitor flash write_image erase build/LawnMower.bin 0x08000000
monitor reset halt
end

define connect
target remote localhost:3333
monitor reset halt
end

connect
file build/LawnMower.elf

define i2c_dump
x 0x40005400
x 0x40005404
x 0x40005408
x 0x4000540c
x 0x40005410
x 0x40005414
x 0x40005418
x 0x4000541c
x 0x40005420
end

define i2c_conf
set {int}0x40005400=0x0
set {int}0x40005420=0x1d
set {int}0x4000541c=0x8c
set {int}0x40005408=0x4000
set {int}0x40005404=0x1c
set {int}0x40005400=0x81
end

define i2c_reset
set {int}0x40005400=0x8000
end

