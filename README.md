# Lawnmower robot
## Intro

This is an intent to make a lawnmower robot.

Note that 90% of the files come from the ST tool for making use of the the STM processors

All the work is done in Core/Src/lawnmower.c and corresponding .h files.
and two lines in freertos.c:
```C
#include "lawnmower.h"
```
at the begining, and:
```
 if(!osThreadNew(main_task,NULL,NULL))
      HAL_GPIO_TogglePin(led1_GPIO_Port, led1_Pin);
```
towards the end.

## Used SW tools:
GCC/GDB for arm cross compiler:
[STM32 Cube MX] (https://www.st.com/en/development-tools/stm32cubemx.html)
for configuring and setting up the microcontroller, also all HAL libraries.

[Official ARM toos] (https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm)
To compile the generated code with STM32.

[OpenOCD] (http://openocd.org)
To download the code to the controller, through a JTAG interface.

## Used hardware

The main board is a so called "industrial" board.
see files Hardware/STM32F103_Ind*
You can find the board from the usual suspects online, searching, for example
"CAN RS485 STM32F103VET6"
One example link:
https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20200417052013&SearchText=CAN+RS485+STM32F103VET6
Note that this board is WAY to much for this project. Any smaller STM32 board will do!

To drive the motors, because the ones I have come from a Husqvarna 305, which
happen to be BLDCs, I've used the boards described in Hardware/JY01_V3.5_2018-English.pdf

There are much cheaper alternatives, if you use some DC motor and H-Bridges.
One example is to use the L298N Modules. Also liste in the Hardware directory.

For the compass I've got the little boards with the HMC5883 knockout, the
QMC5883. The datasheets of both included in the Hardware directory.

Also one little board was used to adapt levels from 3.3V from the controller to
5V of the motor controllers.

Last but not least, one JTAG adapter is needed to program the controller.
I've 2 of them, examples how to use in flash.sh
I have a EM-TECH FTDI based (from company in Argentina)
and one Altera USB Blaster. 

Any one supported by OpenOCD will do.

Note there is included a file .gdbinit to use with GDB, which helped a lot in
debuggin the i2c problems faced at the beginning.
