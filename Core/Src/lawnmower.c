#include "lawnmower.h"
#include "cmsis_os.h"
#include "gpio.h"
#include "tim.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "math.h"

// Constants
#define CONTROL_LIMIT 200
#define VELOCITY 100
#define CAL_SECS 10

// State machine data
enum states {CAL, DRV, FLP, ERR};
enum states state=CAL;

// ADC data
uint16_t adc_buff[1000];

// PID data
int16_t control=0,course=0,course_sec=90;

// Defined functions
float get_heading();
void pid_task(void *task_data);
void pwm_left_motor(int8_t vel);
void pwm_right_motor(int8_t vel);
void initialize();
int8_t rotate_to(int16_t heading,int16_t offset,int timeout);

void main_task(void *task_data)
{
  initialize();
  int16_t pwm;
  while(1)
    {
      switch (state)
	{
	case CAL: // calibration
	  pwm_left_motor(VELOCITY);
	  pwm_right_motor(-VELOCITY);
      	  osDelay(CAL_SECS*1000);
	  state=DRV;
	  break;
	case DRV: // driving (normal mowing)
	  pwm=VELOCITY+control;
	  if (pwm > 100) pwm=100;
	  if (pwm < -100) pwm=-100;
  	  pwm_left_motor(pwm);
	  pwm=VELOCITY-control;
	  if (pwm > 100) pwm=100;
	  if (pwm < -100) pwm=-100;
	  pwm_right_motor(pwm);
	  break;
	case FLP: // FLiPing after a collision
	  // back off 1 second
	  pwm_left_motor(-VELOCITY);
	  pwm_right_motor(-VELOCITY);
	  osDelay(2500);
	  // heading for secondary course
	  if (rotate_to(course_sec,-VELOCITY/2,15))
	    {
	      state=ERR;
	      break;
	    }
	  // move to next "line"
	  pwm_left_motor(VELOCITY);
	  pwm_right_motor(VELOCITY);
	  osDelay(3000);
	  // heading for final course
	  course+=180;
	  course-=(course>=360)?360:0;
	  if (rotate_to(course,VELOCITY/2,15))
	    {
	      state=ERR;
	      break;
	    }
	  // TODO: Here I must reset the PID controller!
	  state=DRV;
	  break;
	case ERR: // Something bad or unexpected: STOP!
	  // Stop motors
	  pwm_left_motor(0);
	  pwm_right_motor(0);
	  // Blink leds
	  HAL_GPIO_WritePin(led1_GPIO_Port, led1_Pin,GPIO_PIN_SET);
	  HAL_GPIO_WritePin(led2_GPIO_Port, led2_Pin,GPIO_PIN_RESET); 
	  while (1)
	    {
	      HAL_GPIO_TogglePin(led1_GPIO_Port, led1_Pin);
	      HAL_GPIO_TogglePin(led2_GPIO_Port, led2_Pin);
	      osDelay(100);
	    }
	  break;
	}
    }
}

void initialize()
{
  // PATCH BUG in STM32F103 I2C implementation:
  uint32_t *i2c_regs=(uint32_t *)0x40005400; //Base peripheral addr.
  *i2c_regs=0x8000; // Set SWRST bit
  osDelay(50); // Wait for correct reset
  *i2c_regs=0x0000; // Take out reset
  *(i2c_regs + 8)=0x1D; // *(i2c_regs + 8)=0x0009; //
  *(i2c_regs + 7)=0x8C; // *(i2c_regs + 7)=0x8018; //
  *(i2c_regs + 2)=0x4000;
  *(i2c_regs + 1)=0x001C;
  osDelay(50); // Wait for configuration to sit
  *i2c_regs=0x0001; // Re-enable the I2C
  // End of Patch

  /* Config QMC8553 see datasheet */
  HAL_StatusTypeDef status;
  uint8_t i2c_buff[2];
  i2c_buff[0]=0x0B; i2c_buff[1]=0x01;
  HAL_ADC_Start_DMA(&hadc1,(uint32_t*)adc_buff,1000);
  status=HAL_I2C_Master_Transmit(&hi2c1,0x1A,i2c_buff,2,HAL_MAX_DELAY);
  if (status!=0) state=ERR;
  i2c_buff[0]=0x0A; i2c_buff[1]=0x40;
  status=HAL_I2C_Master_Transmit(&hi2c1,0x1A,i2c_buff,2,HAL_MAX_DELAY);
  if (status!=0) state=ERR;
  i2c_buff[0]=0x09; i2c_buff[1]=0x01;
  status=HAL_I2C_Master_Transmit(&hi2c1,0x1A,i2c_buff,2,HAL_MAX_DELAY);
  if (status!=0) state=ERR;
  i2c_buff[0]=0x00;
  status=HAL_I2C_Master_Transmit(&hi2c1,0x1A,i2c_buff,1,HAL_MAX_DELAY);
  if (status!=0) state=ERR;
  // Start the PID task in background
  osThreadNew(pid_task,NULL,NULL);
}


void pwm_left_motor(int8_t vel)
{
  if (vel<0)
    {
      HAL_GPIO_WritePin(left_dir_GPIO_Port, left_dir_Pin,GPIO_PIN_RESET);
      vel*=-1;
    }
  else
    {
      HAL_GPIO_WritePin(left_dir_GPIO_Port, left_dir_Pin,GPIO_PIN_SET);
    }
    
  TIM_OC_InitTypeDef sConfigOC = {0};
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = vel*10;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
}

void pwm_right_motor(int8_t vel)
{
  if (vel<0)
    {
      HAL_GPIO_WritePin(right_dir_GPIO_Port, right_dir_Pin,GPIO_PIN_RESET);
      vel*=-1;
    }
  else
    {
      HAL_GPIO_WritePin(right_dir_GPIO_Port, right_dir_Pin,GPIO_PIN_SET);
    }
    
  TIM_OC_InitTypeDef sConfigOC = {0};
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = vel*10;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
}

int8_t rotate_to(int16_t heading,int16_t offset,int timeout)
{
  // get ticks
  uint32_t ticks=osKernelGetTickCount()+timeout*osKernelGetTickFreq();
  float angle;
  do
    {
      angle=heading-get_heading();
      if (angle > 180) angle-=360;
      if (angle < -180) angle+=360;
      if (angle > 10) //turn right
	{
	  pwm_left_motor(VELOCITY/2+offset);
	  pwm_right_motor(-VELOCITY/2+offset);
	}
      if (angle < -10) //turn left
	{
	  pwm_left_motor(-VELOCITY/2+offset);
	  pwm_right_motor(VELOCITY/2+offset);
	}
      if(osKernelGetTickCount()>ticks)
	return(1); // TIMEOUT!
      osDelay(100);
    } while ( (angle < -10) || (angle > 10) );
    return(0);
}

void pid_task(void *task_data)
{
  static float integral,last_error,derivate;
  static float kp=1,ki=0.01,kd=0.1;
  
  while(1)
    {
      float heading=get_heading();
    
      float error=heading-course;
      if (error > 180) error-=360;
      if (error < -180) error+=360;
      
      integral+=error;
      derivate=error-last_error;

      control=-kp*error-kd*derivate-ki*integral;
      
      if (control > CONTROL_LIMIT) control=CONTROL_LIMIT;
      if (control < -CONTROL_LIMIT) control=-CONTROL_LIMIT;
      
      osDelay(100);
    }
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  //HAL_ADC_Start_DMA(&hadc1,(uint32_t*)adc_buff,1000);
  HAL_ADC_Stop_DMA(&hadc1);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  /* if (state!=DRV) */
  /*   { */
  /*     state=ERR; */
  /*     return; */
  /*   } */
  state=FLP;
  if (GPIO_Pin ==left_whisker_Pin)
    {
    }
  if (GPIO_Pin ==right_whisker_Pin)
    {
    }  
}

float get_heading()
{
  static int16_t x_min=32767, x_max=-32767, y_min=32767, y_max=-32767;
  int16_t mag_x,mag_y;

  HAL_StatusTypeDef status;
  uint8_t i2c_buff[7];
  status=HAL_I2C_Master_Receive(&hi2c1,0x1A,i2c_buff,7,HAL_MAX_DELAY);
  if (status!=0) state=ERR;
  mag_x = *((int16_t *) i2c_buff);
  mag_y = *((int16_t *) i2c_buff+1);
  
  if (mag_x<x_min) x_min=mag_x;
  if (mag_y<y_min) y_min=mag_y;
  if (mag_x>x_max) x_max=mag_x;
  if (mag_y>y_max) y_max=mag_y;
  
  float x_avg=(x_max+x_min)/2;
  float x_rng=(x_max-x_min)/2;
  float y_avg=(y_max+y_min)/2;
  float y_rng=(y_max-y_min)/2;

  float x=(mag_x-x_avg)/x_rng;
  float y=(mag_y-y_avg)/y_rng;

  float heading=0;
  if ((x>=0) && (y>=0))
    {
      if (x>y)
	{
	  heading=(180/M_PI)*atan(y/x);
	}
      else
	{
	  heading=90-(180/M_PI)*atan(x/y);
	}
    }
  if ((x<0) && (y>=0))
    {
      if (x>y)
	{
	  heading=180-(180/M_PI)*atan(y/-x);
	}
      else
	{
	  heading=90+(180/M_PI)*atan(-x/y);
	}
    }
  if ((x<0) && (y<0))
    {
      if (x>y)
	{
	  heading=180+(180/M_PI)*atan(-y/-x);
	}
      else
	{
	  heading=270-(180/M_PI)*atan(-x/-y);
	}
    }
  if ((x>=0) && (y<0))
    {
      if (x>y)
	{
	  heading=360-(180/M_PI)*atan(-y/x);
	}
      else
	{
	  heading=270+(180/M_PI)*atan(x/-y);
	}
    }
  return(heading);
}
